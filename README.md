
# vhostadm

A tool for managing configuration with the Nginx web server.

Specifically:

 - This tool manages per-vhost config files assuming the Debian
   config layout (/etc/nginx/{sites-available|sites-enabled}).
 - This tool is intended to be installed setuid root.
 - The tool should be callable by either a human admin or some sort
   of automated management system.

Stuff we want to be able to do:

 - List all configs
 - Enable or disable configs
 - Generate configs based on templates
 - Regenerate configs preserving certbot settings
 - Restart nginx
 - Rerun acme.sh

## License

vhostadm
Copyright (C) 2021 Nat Tuck

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.

