
use clap::Clap;

#[derive(Clap, Debug)]
#[clap(version = "0.1", author = "Nat Tuck <nat@ferrus.net>")]
pub struct Opts {
    #[clap(subcommand)]
    pub action: Action,
}

#[derive(Clap, Debug)]
pub enum Action {
    List,
    Enable(Enable),
    Disable(Disable),
    Create(Create),
    GenDHP,
}

#[derive(Clap, Debug)]
pub struct Enable {
    pub host: String,
}

#[derive(Clap, Debug)]
pub struct Disable {
    pub host: String,
}

#[derive(Clap, Debug)]
pub struct Create {
    pub host: String,
}
