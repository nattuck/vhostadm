
use std::{fs, ffi};

pub trait GimmieString {
    fn to_string(&self) -> String;
}

impl GimmieString for ffi::OsString {
    fn to_string(&self) -> String {
        let temp = self.to_string_lossy();
        temp.to_string()
    }
}

impl GimmieString for fs::DirEntry {
    fn to_string(&self) -> String {
        self.file_name().to_string()
    }
}
