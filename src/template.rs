
use anyhow::Result;

static TMPL: &str = r##"
server {
    listen 80;
    listen [::]:80;

    server_name {{ hostname }};

    return 302 https://$host$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name {{ hostname }};
    root {{ docroot }};
    index index.html;

    ssl_certificate {{ cert_path }};
    ssl_certificate_key {{ key_path }};

    ssl_dhparam {{ dhparams_path }}
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
    ssl_prefer_server_ciphers on;
}
"##;

pub fn render(vars: &liquid::Object) -> Result<String> {
    let tmpl = liquid::ParserBuilder::with_stdlib()
        .build()?.parse(TMPL)?;
    let text = tmpl.render(vars)?;
    Ok(text)
}
