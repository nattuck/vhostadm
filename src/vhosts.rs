
use std::{fmt, fs};
use std::collections::BTreeMap;
use std::path::PathBuf;

use anyhow::Result;

use crate::traits::GimmieString;
use crate::config;

pub struct Host {
    pub name: String,
    pub docroot: String,
    pub enabled: bool,
}

impl fmt::Display for Host {
    fn fmt(&self, ff: &mut fmt::Formatter) -> fmt::Result {
        write!(ff, "{}", &self.name)
    }
}

pub fn conf_base() -> PathBuf {
    PathBuf::from(config::get("CONF_BASE"))
}

pub fn list() -> Result<Vec<Host>> {
    let mut mm: BTreeMap<String, Host> = BTreeMap::new();
    let mut ys = vec![];

    let av_path = conf_base().join("sites-available");
    for ent in fs::read_dir(av_path)? {
        let name = ent?.to_string();
        println!("av: {}", name);
    }

    let en_path = conf_base().join("sites-enabled");
    for ent in fs::read_dir(en_path)? {
        let name = ent?.to_string();
        println!("en: {}", name);
    }

    Ok(ys)
}

pub fn create(host: &str) -> Result<()> {
    let vars = liquid::object!({
        "hostname": host,
        "docroot": "/home/bob/www",
        "cert_path": "/etc/nginx/cert-path",
        "key_path": "/etc/nginx/key-path",
        "dhparams_path": "/etc/nginx/key-path",
    });

    let text = crate::template::render(&vars)?;
    println!("{}", text);

    Ok(())
}
