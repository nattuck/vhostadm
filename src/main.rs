
use std::process::Command;

use clap::Clap;
use anyhow::Result;

mod opts;
use opts::{Opts, Action};

mod config;
mod vhosts;
mod template;
mod traits;

fn main() -> Result<()> {
    let opts: Opts = Opts::parse();

    match &opts.action {
        Action::List => {
            let hosts = vhosts::list()?;
            for host in hosts {
                println!("{}", host);
            }
        },
        Action::Create(info) => {
            vhosts::create(&info.host)?;
        },
        Action::GenDHP => {
            let path = vhosts::conf_base().join("tls/dhparams.pem");
            Command::new("openssl")
                .arg("dhparam")
                .arg("-out")
                .arg(path)
                .arg("2048")
                .status()?;
        },
        _ => {
            println!("other: {:?}", &opts);
        }
    }

    Ok(())
}
