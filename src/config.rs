
use std::sync::{Arc, Mutex};
use std::collections::BTreeMap;
use std::env;

use lazy_static::lazy_static;

use crate::traits::GimmieString;

lazy_static! {
    static ref CONF: Arc<Mutex<BTreeMap<String, String>>> =
        Arc::new(Mutex::new(defaults()));
}

fn defaults() -> BTreeMap<String, String> {
    let mut mm = BTreeMap::new();
    let base_default = env::var_os("CONF_BASE").unwrap_or("/etc/nginx".into());
    mm.insert("CONF_BASE".into(), base_default.to_string());
    mm
}

pub fn get(name: &str) -> String {
    let mm = CONF.lock().unwrap();
    if let Some(text) = mm.get(name) {
        text.into()
    }
    else {
        "".into()
    }
}

pub fn set(name: &str, value: &str) {
    let mut mm = CONF.lock().unwrap();
    mm.insert(name.into(), value.into());
}
